#!/bin/bash
#storent-loadnt-bypssreq-bypassfill
cache=${1}
folder="results_parse"
for trace in `ls traces`; do 
echo ""
#for trace in `ls traces | grep -a -v poly`; do 
for version in "\-0000" "p${cache}" "\-1101" "\-1110" "\-1111"; do
	file=`ls $folder | grep -a $trace | grep -a $cache | grep -a $version`
	parsed=`cat $folder/$file | grep -a "parseable,cycles" | cut -d',' -f3-4`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a L1D | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a L2C | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a LLC | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a NTB | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a DRAM | cut -f3-6 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a HBPB | cut -f3-8 -d','`
	printf $parsed
	printf '\n'
done
done
