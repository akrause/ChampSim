file=${1}
linha=0
while IFS= read line
do
	let "linha = linha + 1"
    echo "$linha $line"
	sbatch --job-name="b-b-$linha" runbatch.multi.sert base.base 1999 $linha $line spec17
	sbatch --job-name="b-p-$linha" runbatch.multi.sert base.pref 1999 $linha $line spec17
	sbatch --job-name="h-b-$linha" runbatch.multi.sert hbpb.base 1999 $linha $line spec17
	sbatch --job-name="h-p-$linha" runbatch.multi.sert hbpb.pref 1999 $linha $line spec17
	sbatch --job-name="r-b-$linha" runbatch.multi.sert redu.base 1999 $linha $line spec17
	sbatch --job-name="r-p-$linha" runbatch.multi.sert redu.pref 1999 $linha $line spec17
done <"$file"
