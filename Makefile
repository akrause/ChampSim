app = champsim

srcExt = cc
srcDir = src branch replacement prefetcher
objDir = obj
binDir = bin
inc = inc

debug = 1

CFlags = -O3 -std=c++11
#CFlags = -Og -std=c++11
#CFlags = -ggdb3 -std=c++11 -DDEBUG_ARTHUR
#CFlags = -ggdb3 -std=c++11 
LDFlags =
libs =
libDir =


#************************ DO NOT EDIT BELOW THIS LINE! ************************

ifeq ($(debug),1)
	debug=-g
else
	debug=
endif
inc := $(addprefix -I,$(inc))
libs := $(addprefix -l,$(libs))
libDir := $(addprefix -L,$(libDir))
CFlags += -c $(debug) $(inc) $(libDir) $(libs)
sources := $(shell find $(srcDir) -name '*.$(srcExt)')
srcDirs := $(shell find . -name '*.$(srcExt)' -exec dirname {} \; | uniq)
objects := $(patsubst %.$(srcExt),$(objDir)/%.o,$(sources))
deps := $(patsubst %.$(srcExt),$(objDir)/%.d,$(sources))

ifeq ($(srcExt),cc)
	CC = $(CXX)
else
	CFlags += -std=gnu99
endif

.phony: all clean distclean

all: $(binDir)/$(app)

$(binDir)/$(app): buildrepo $(objects)
	@mkdir -p `dirname $@`
	@echo "Linking $@..."
	@echo "@$(CC) $(objects) $(LDFlags) -o $@"
	@$(CC) $(objects) $(LDFlags) -o $@

$(objDir)/%.o: $(objDir)/%.d
#$(objDir)/%.o: $(deps) 
	@echo "Compiling $<..."
	@echo "@$(CC) $(CFlags) $(subst .d,.$(srcExt),$(subst $(objDir)/,,$@)) -o $@"
	@$(CC) $(CFlags) $(subst .o,.$(srcExt),$(subst $(objDir)/,,$@)) -o $@

$(objDir)/%.d: %.$(srcExt)
	@echo "Generating dependencies for $<..."
	@$(call make-depend,$<,$(subst .d,.o,$@),$@)
#	@echo "Finished dependencies for $<..."


clean:
	$(RM) -r $(objDir)

distclean: clean
	$(RM) -r $(binDir)/$(app)

buildrepo:
	@$(call make-repo)

define make-repo
   for dir in $(srcDirs); \
   do \
	mkdir -p $(objDir)/$$dir; \
   done
endef


# usage: $(call make-depend,source-file,object-file,depend-file)
define make-depend
  $(CC) -MM       \
        -MF $3    \
        -MP       \
        -MT $2    \
        $(CFlags) \
        $1
endef
