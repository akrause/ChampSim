#!/bin/bash
#ins=9999
ins=9999999
microsdir=$HOME/ChampSimTraces/micros
tracedir=$HOME/tmp/ChampSim/ML-DPC/ChampSimTraces/
loadtracedir=$HOME/tmp/ChampSim/ML-DPC/LoadTraces/

size="head"

#for cache in 1024 1280 8192 512; do
#for replacement in drrip ship srrip; do
#for cache in 832 1024 1744 2048 7168 8192; do
	#for size in "tail"; do
	#for size in "head" ; do
#for suite in spec17 gap spec06; do
for replacement in lru drrip srrip ship; do
	for suite in spec17 gap; do
		for cache in 1024 832 2048 1744 8196 7168; do
			vec=`ls $tracedir/$suite | cut -f1 -d-`
			for unique in `echo "${vec[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '`; do
				#echo $unique
				if [ $suite = "spec17" ]; then
					trace=`ls -S $loadtracedir/$suite | grep $unique | $size -1 | sed 's/txt\.xz/trace\.xz/g'`
				else
					trace=`ls -S $loadtracedir/$suite | grep $unique | $size -1 | sed 's/txt\.xz/trace\.gz/g'`
				fi
				for bin in `ls bin | grep $replacement | grep $cache`; do
					echo "sbatch --job-name="$bin-$trace-$suite" runbatch.shared2 $bin $ins $trace ilim $suite "
					sbatch --job-name="$bin-$trace-$suite" runbatch.shared2 $bin $ins $trace ilim $suite
					echo $bin,$suite,$trace,$size
				done
			done
		done
	done
#done
suite=micros
#for replacement in lru drrip srrip ship; do
	for trace in `ls $microsdir`; do
	#for replacement in lru ; do
		for cache in 1024 832 2048 1744 8196 7168; do
		#for cache in 1024; do
			for bin in `ls bin | grep $replacement | grep $cache`; do
				if [[ $trace == *"dep"* ]]; then
					sbatch --job-name="$bin-$suite-$trace" runbatch.shared2 $bin 100 $trace not $suite
					echo "sbatch --job-name="$bin-$suite-$trace" runbatch.shared2 $bin 100 $trace not $suite"
				else
					sbatch --job-name="$bin-$suite-$trace" runbatch.shared2 $bin $ins $trace ilim $suite
					echo "sbatch --job-name="$bin-$suite-$trace" runbatch.shared2 $bin $ins $trace ilim $suite"
				fi
				echo $bin,$suite,$trace,$size
			done
		done
		
	done	
done
