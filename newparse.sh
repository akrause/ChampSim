# re/bin/bash
#storent-loadnt-bypssreq-bypassfill
cache="512"
cache=${2}
cache2=${3}

if [ -z $cache2 ]; then
	cache2=$cache
fi
folder=${1}
#echo $folder
printf '"app","pref_t","pref_n","mech","replacement","cache",'
printf '"instructions","cycles","L1_fill","L1_read","L1_miss","L2_fill","L2_read","L2_miss","L3_fill","L3_read","L3_miss","NTB_fill","NTB_read","NTB_miss","DRAM_rq_rb_hit_1","DRAM_rq_rb_miss_1","DRAM_wq_rb_hit_1","DRAM_wq_rb_miss_1","DRAM_rq_rb_hit_2","DRAM_rq_rb_miss_2","DRAM_wq_rb_hit_2","DRAM_wq_rb_miss_2","HBPB_load","HBPB_store","HBPB_nt_load","HBPB_nt_store","HBPB_hit","HBPB_miss","WQ_full","not_filled"'
printf '\n'
for result in `ls $folder`; do
	if [[ "$folder" == *"spec"* ]]; then
		app=$( echo $result | cut -f2 -d'.' | cut -f1 -d'-') #SPEC
	elif [[ "$folder" == *"gap"* ]]; then
		app=$( echo $result | cut -f1 -d'.' | cut -f1 -d'-' ) #GAP
	elif [[ "$folder" == *"micros"* ]]; then
		if ( [[ "$result" == *"yolo"* ]] || [[ "$result" == *"dist"* ]] ); then
			app=$( echo $result | cut -f1 -d'.' ) #MICROS
		else
			app=$( echo $result | cut -f1-3 -d'.' ) #MICROS
		fi
	fi
	pref=$( echo $result | awk -v FS=. '{ print $(NF-4) }' | cut -f2 -d- )
	pref_t=${pref::1}
	pref_n=${pref: -1}
	mech=$( echo $result | awk -v FS=. '{ print $(NF-3) }' | cut -f2 -d- )
	mech=${mech:2:2}
	repl=$( echo $result | awk -v FS=. '{ print $(NF-2) }' | cut -f2 -d- )
	cache=$( echo $result | awk -v FS=. '{ print $(NF-1) }' | cut -f2 -d- )
	
	printf $app,$pref_t,$pref_n,$mech,$repl,$cache","
    
	parsed=`cat $folder/$result | grep -a "parseable,cycles" | cut -d',' -f3-4`
	printf $parsed","
	parsed=`cat $folder/$result | grep -a parseable | grep -a L1D | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$result | grep -a parseable | grep -a L2C | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$result | grep -a parseable | grep -a LLC | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$result | grep -a parseable | grep -a NTB | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$result | grep -a parseable | grep -a DRAM | cut -f3-6 -d',' | sed 'N;s/\n/,/'`
	printf $parsed","
	#parsed=`cat $folder/$file | grep -a parseable | grep -a HBPB | cut -f3-8 -d','`
	parsed=`cat $folder/$result | grep -a parseable | grep -a HBPB | cut -f3-10 -d','`
	printf $parsed
	printf '\n'

done

exit




for trace in `ls $folder | grep "11.1111.lru.1024" | cut -f1 -d'-'`; do 
echo "$trace"
echo " "
#for trace in `ls traces | grep -a -v poly`; do 
folder=`echo $folder | sed "s/$cache2/$cache/g"`
for version in "\-0000" "\-p" ; do
	file=`ls $folder | grep -a $trace | grep -a $version`
#	printf $version" - "
	parsed=`cat $folder/$file | grep -a "parseable,cycles" | cut -d',' -f3-4`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a L1D | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a L2C | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a LLC | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a NTB | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a DRAM | cut -f3-6 -d',' | sed 'N;s/\n/,/'`
	printf $parsed","
	#parsed=`cat $folder/$file | grep -a parseable | grep -a HBPB | cut -f3-8 -d','`
	parsed=`cat $folder/$file | grep -a parseable | grep -a HBPB | cut -f3-9 -d','`
	printf $parsed
	printf '\n'
done
folder=`echo $folder | sed "s/$cache/$cache2/g"`
for version in "\-1101" "\-1110" "\-1111"; do
	file=`ls $folder | grep -a $trace | grep -a $version`
#	printf $version" - "
	parsed=`cat $folder/$file | grep -a "parseable,cycles" | cut -d',' -f3-4`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a L1D | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a L2C | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a LLC | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a NTB | cut -f3-5 -d','`
	printf $parsed","
	parsed=`cat $folder/$file | grep -a parseable | grep -a DRAM | cut -f3-6 -d',' | sed 'N;s/\n/,/'`
	printf $parsed","
	#parsed=`cat $folder/$file | grep -a parseable | grep -a HBPB | cut -f3-8 -d','`
	parsed=`cat $folder/$file | grep -a parseable | grep -a HBPB | cut -f3-9 -d','`
	printf $parsed
	printf '\n'
done
done
