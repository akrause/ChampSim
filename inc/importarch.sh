#!/bin/bash

HERMES_FILE=$1

while read -r line
do
	if [[ $line == "#define"* ]]; then
		#line=$(echo $line | awk -F' +|//' '/^[^#]/{print $1}')
		line=$(echo $line | awk -F'//' '{print $1}')
		echo $line
		field=$(echo $line | awk -F'[ ]+' '{print $2}')
		echo $field
		value=$(echo $line | awk -F'[ ]+' '{print $3}')
		echo $value
		echo " "
		value=$(echo $value | sed 's/\//\\\//g')
		value=$(echo $value | sed 's/\*/\\\*/g')

		for file in `ls | grep .h`; do
			sed -i "s/#define $field.*"/"#define $field $value/g" $file
		done
	fi
done < $HERMES_FILE
