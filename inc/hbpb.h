#pragma once
#include "cache.h"
#define PC_TABLE_SIZE 512
#define TEMPORAL_MAX 32
#define TEMPORAL_MIN 1
#define TEMPORAL_THRESHOLD 16
#define HBPB_EVICT -1
#define HBPB_HIT 2


class HBPBTABLE : public CACHE {
  public:
    // constructor
    HBPBTABLE(string v1, uint32_t v2, int v3, uint32_t v4, uint32_t v5, uint32_t v6, uint32_t v7, uint32_t v8)
    : CACHE (v1, v2, v3, v4, v5, v6, v7, v8) {
      cache_type = 8;
    } ;

    uint64_t insert(PACKET* packet); //inserts an element and returns the ip of the evicted element
    uint64_t insert(uint64_t address); //inserts an element and returns the ip of the evicted element

    uint64_t check_hit_hbpb(uint64_t address); //uses the address directly instead of the packet and returns the ip of the element
    uint64_t check_hit(uint64_t address);
    uint8_t check_temporal(uint64_t ip);
    uint8_t check_zero(uint64_t ip);
    void add_counter(uint64_t ip, int value);


};

class HBPB_CONTROLLER : public MEMORY {
  public:
    const string NAME;

    CACHE NTB{"NTB", NTB_SET, NTB_WAY, NTB_SET*NTB_WAY, NTB_WQ_SIZE, NTB_RQ_SIZE, NTB_PQ_SIZE, NTB_MSHR_SIZE};
    HBPBTABLE ADD_TABLE{"ADD_TABLE", LLC_SET, LLC_WAY, LLC_SET*LLC_WAY, 1, 1, 1, 1};
    HBPBTABLE PC_TABLE{"PC_TABLE", 1, PC_TABLE_SIZE, PC_TABLE_SIZE, 1, 1, 1, 1};
    uint64_t t_load[NUM_CPUS], t_store[NUM_CPUS], nt_load[NUM_CPUS], nt_store[NUM_CPUS], hit[NUM_CPUS], miss[NUM_CPUS], wq_full[NUM_CPUS], not_filled[NUM_CPUS];
    uint64_t t_load_roi[NUM_CPUS], t_store_roi[NUM_CPUS], nt_load_roi[NUM_CPUS], nt_store_roi[NUM_CPUS], hit_roi[NUM_CPUS], miss_roi[NUM_CPUS], wq_full_roi[NUM_CPUS], not_filled_roi[NUM_CPUS];



    HBPB_CONTROLLER(string v1) : NAME (v1)
    {
      for (int i = 0; i < NUM_CPUS; i++) {
        t_load[i] = t_store[i] = nt_load[i] = nt_store[i] = hit[i] = miss[i] =  wq_full[i] = not_filled[i] = 0;
        t_load_roi[i] = t_store_roi[i] = nt_load_roi[i] = nt_store_roi[i] = hit_roi[i] = miss_roi[i] =  wq_full_roi[i] = not_filled_roi[i] = 0;
      }
    }

  //onde chamar isso? a cada acesso que é criado?
  //  void process_hbpb(PACKET* packet);
    int add_rq(PACKET *packet);

    //just to make the class not virtual
    int  add_wq(PACKET *packet);
    int  add_pq(PACKET *packet);
    void return_data(PACKET *packet);
    void operate();
    void increment_WQ_FULL(uint64_t address);
    uint32_t get_occupancy(uint8_t queue_type, uint64_t address);
    uint32_t get_size(uint8_t queue_type, uint64_t address);
    int invalidate_entry(uint64_t inval_addr);

};
