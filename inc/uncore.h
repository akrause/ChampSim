#ifndef UNCORE_H
#define UNCORE_H

#include "champsim.h"
#include "cache.h"
#include "dram_controller.h"
#include "hbpb.h"
//#include "drc_controller.h"

//#define DRC_MSHR_SIZE 48

// uncore
class UNCORE
{
public:
  // LLC
  CACHE LLC{"LLC", LLC_SET, LLC_WAY, LLC_SET *LLC_WAY, LLC_WQ_SIZE, LLC_RQ_SIZE, LLC_PQ_SIZE, LLC_MSHR_SIZE};

  // ARTHUR
  //CACHE NTB{"NTB", L1D_SET, L1D_WAY, L1D_SET*L1D_WAY, L1D_WQ_SIZE, L1D_RQ_SIZE, L1D_PQ_SIZE, LLC_MSHR_SIZE};

  HBPB_CONTROLLER HBPB{"HBPB_CONTROLLER"};

  // DRAM
  MEMORY_CONTROLLER DRAM{"DRAM"};

  UNCORE();
};

extern UNCORE uncore;

#endif
