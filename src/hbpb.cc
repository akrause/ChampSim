#include "hbpb.h"

uint64_t HBPBTABLE::check_hit(uint64_t address)
{ //overload that uses only the address and not the packet
  uint32_t set = get_set(address);
  int match_way = -1;
  // hit
  for (uint32_t way = 0; way < NUM_WAY; way++)
  {
    if (block[set][way].valid && (block[set][way].tag == address))
    {
      match_way = way;
      break;
    }
  }
  return match_way;
}

uint64_t HBPBTABLE::check_hit_hbpb(uint64_t address)
{
  uint32_t set = get_set(address);
  int match_way = -1;
  // hit
  for (uint32_t way = 0; way < NUM_WAY; way++)
  {
    if (block[set][way].valid && (block[set][way].tag == address))
    {
      match_way = way;
      break;
    }
  }
  if (match_way == -1)
    return 0;
  else
    return block[set][match_way].data;
}

void HBPBTABLE::add_counter(uint64_t ip, int value)
{
  uint32_t set = get_set(ip);
  uint32_t way = check_hit(ip);

  if (way != -1)
  {
    block[set][way].data += value;
    if (block[set][way].data > TEMPORAL_MAX)
      block[set][way].data = TEMPORAL_MAX;
    else if (block[set][way].data < TEMPORAL_MIN)
      block[set][way].data = TEMPORAL_MIN;
  }
  update_replacement_state(0, set, way, 0, 0, 0, 0, 0);
}

uint8_t HBPBTABLE::check_temporal(uint64_t ip)
{
  uint64_t value = check_hit_hbpb(ip);
  if (value > 0)
  {
    if (value > TEMPORAL_THRESHOLD)
    {
      block[get_set(ip)][check_hit(ip)].instr_id++;
      return 1;
    }
    else
    {
      block[get_set(ip)][check_hit(ip)].ip++;
      return 0;
    }
  }
  else
    return 0;
}

uint8_t HBPBTABLE::check_zero(uint64_t ip)
{
  uint64_t value = check_hit_hbpb(ip);
  if (value > 0)
  {
    if (value == TEMPORAL_MIN)
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }
  else
    return 0;
}

uint64_t HBPBTABLE::insert(PACKET *packet)
{ //to be used on ADD_TABLE
  uint32_t set = get_set(packet->address), way;
  uint64_t evicted_ip;

  way = CACHE::check_hit(packet);
  if (way == -1)
  {
    way = llc_find_victim(packet->cpu, packet->instr_id, set, block[set], packet->cpu_and_ip, packet->full_addr, packet->type);
    evicted_ip = block[set][way].data;
    llc_update_replacement_state(packet->cpu, set, way, block[set][way].full_addr, packet->cpu_and_ip, 0, packet->type, 0);
  }
  else
  {
    evicted_ip = 0;
    llc_update_replacement_state(packet->cpu, set, way, block[set][way].full_addr, packet->cpu_and_ip, 0, packet->type, 1);
  }
  fill_cache(set, way, packet);
  block[set][way].data = packet->cpu_and_ip;
  return evicted_ip;
}

uint64_t HBPBTABLE::insert(uint64_t address)
{ //to be used on PC_TABLE
  uint32_t set = get_set(address), way;
  way = find_victim(0, 0, set, block[set], 0, 0, 0);
  uint64_t evicted_ip = block[set][way].data;
  //fill_cache(set, way, packet);
  block[set][way].data = TEMPORAL_THRESHOLD + 1;
  update_replacement_state(0, set, way, 0, 0, 0, 0, 0);
  block[set][way].valid = 1;
  block[set][way].tag = address;
  block[set][way].instr_id = 0; //temporal
  block[set][way].ip = 0;       //nt
  return evicted_ip;
}

int HBPB_CONTROLLER::add_rq(PACKET *packet)
{
  uint64_t hit_address, evicted_address;

  evicted_address = 0;

	if( LOAD_NT || STORE_NT) {

  hit_address = ADD_TABLE.check_hit_hbpb(packet->address); //retorna 0 se da miss
  if ((hit_address == 0) && (PC_TABLE.check_hit(packet->cpu_and_ip) != -1)) //miss na add table e hit na pc_table

  // if(PC_TABLE.check_hit(packet->cpu_and_ip) != -1)
  {
    if((PC_TABLE.check_zero(packet->cpu_and_ip) == 0) || (rand()%32 == 0)) //se ta nao ta saturado negativamente ou deu sorte
    //if ((PC_TABLE.check_zero(packet->cpu_and_ip) == 0))
      evicted_address = ADD_TABLE.insert(packet); //insere na tabela
    else
    {
      not_filled[packet->cpu]++;
    }
  }
  else 
    evicted_address = ADD_TABLE.insert(packet); //

  if (hit_address != 0)
  { // HBPB HIT
    hit[packet->cpu]++;
    if (PC_TABLE.check_hit(hit_address) != uint64_t(-1)) // computes for the pc that was hit on the table
      PC_TABLE.add_counter(hit_address, HBPB_HIT);       //increment temporality counter from pc of the address that was hit on the table
    else
      PC_TABLE.insert(hit_address);

    if (PC_TABLE.check_hit(packet->cpu_and_ip) != uint64_t(-1)) //computes for the new pc, that hitted on the table
      PC_TABLE.add_counter(packet->cpu_and_ip, HBPB_HIT);       //increment temporality counter from pc of the address that hitted on the table
    else
      PC_TABLE.insert(packet->cpu_and_ip);
  }
  else
  { // HBPB MISS
    miss[packet->cpu]++;
    if (evicted_address != 0)
      if (PC_TABLE.check_hit(evicted_address) != uint64_t(-1))
      {                                                    // computes for the pc that was evicted on the table
        PC_TABLE.add_counter(evicted_address, HBPB_EVICT); //decrement temporality counter from pc of the address that was evicted from the table
      }
      else
      {
        PC_TABLE.insert(evicted_address);
        PC_TABLE.add_counter(evicted_address, HBPB_EVICT);
      }
      if (PC_TABLE.check_hit(packet->cpu_and_ip) != uint64_t(-1)) //tentativa: se da miss e ta na tabela, decrementa
      {                                                    
        //PC_TABLE.add_counter(packet->cpu_and_ip, HBPB_EVICT); 
		;
      }
		
  }
}

  packet->is_nontemporal = 0;
  packet->bypass_request = 0;
  packet->bypass_fill = 0;
  uint8_t temporal = PC_TABLE.check_temporal(packet->cpu_and_ip);
  if (packet->type == LOAD)
    if (LOAD_NT && !temporal)
    {
      nt_load[packet->cpu]++;
      packet->is_nontemporal = 1;
      if (BYPASS_REQ)
        packet->bypass_request = 1;
      if (BYPASS_FILL)
        packet->bypass_fill = 1;
    }
    else
    {
      t_load[packet->cpu]++;
    }

  if (packet->type == RFO)
    if (STORE_NT && !temporal)
    {
      nt_store[packet->cpu]++;
      packet->is_nontemporal = 1;
      if (BYPASS_REQ)
        packet->bypass_request = 1;
      if (BYPASS_FILL)
        packet->bypass_fill = 1;
    }
    else
    {
      t_store[packet->cpu]++;
    }

  if (packet->is_nontemporal)
  {
    PACKET parallel_packet = *packet;
    parallel_packet.is_parallel = 1;
    if (packet->type == RFO)
    {
      if (packet->bypass_request && packet->bypass_fill)
      { //bypass de request, manda o NTB antecipar o LOAD
        parallel_packet.type = LOAD;
        NTB.add_rq(&parallel_packet);
        if (NTB.get_occupancy(2, packet->address) < NTB.get_size(2, packet->address))
        {
          parallel_packet.type = RFO;
          NTB.add_wq(&parallel_packet);
        }
        else
        {
          wq_full[packet->cpu]++;
        }
      }
      else if (packet->bypass_fill)
      { //bypass de fill, manda um write pro NTB
        if (NTB.get_occupancy(2, packet->address) < NTB.get_size(2, packet->address))
        {
          NTB.add_wq(&parallel_packet);
        }
        else
        {
          wq_full[packet->cpu]++;
        }
      }
      else if (packet->bypass_request)
      {
        parallel_packet.type = LOAD;
        NTB.add_rq(&parallel_packet);
      }
    }
    else if (packet->type == LOAD)
    {
      if (packet->bypass_request)
      {
        PACKET parallel_packet = *packet;
        parallel_packet.is_parallel = 1;
        NTB.add_rq(&parallel_packet);
      }
    }
  }
  return 0;
}

int HBPB_CONTROLLER::add_wq(PACKET *packet)
{
  return -1;
}
int HBPB_CONTROLLER::add_pq(PACKET *packet)
{
  return -1;
}
void HBPB_CONTROLLER::return_data(PACKET *packet)
{
  if (PC_TABLE.check_hit(packet->cpu_and_ip) != -1)
    PC_TABLE.add_counter(packet->cpu_and_ip, 2 * HBPB_HIT);
}
void HBPB_CONTROLLER::operate()
{
}
void HBPB_CONTROLLER::increment_WQ_FULL(uint64_t address)
{
}
uint32_t HBPB_CONTROLLER::get_occupancy(uint8_t queue_type, uint64_t address)
{
  return -1;
}
uint32_t HBPB_CONTROLLER::get_size(uint8_t queue_type, uint64_t address)
{
  return -1;
}
int HBPB_CONTROLLER::invalidate_entry(uint64_t inval_addr)
{
  return -1;
}
