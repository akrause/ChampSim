#include "block.h"

int PACKET_QUEUE::check_queue(PACKET *packet)
{
#ifdef DEBUG_ARTHUR
    cout << "recebeu check queue " << NAME << " address: " << packet->address << " occupancy: " << occupancy << endl;

    //if(NAME == "NTB_WQ" && (occupancy >= SIZE))
    /*
  if(NAME == "NTB_WQ" )
  {
    cout << "ciclo: " << current_core_cycle[packet->cpu] << " head: " << head << endl;
    for (uint32_t j=0; j<SIZE; j++) {
        cout << " NTB [WQ] entry: " << j << " instr_id: " << entry[j].instr_id << " address: " << entry[j].address << " full_address: " << entry[j].full_addr << " event_cycle: " << entry[j].event_cycle << endl;
    }
  }
  */

#endif

    if ((head == tail) && occupancy == 0)
        return -1;

    if (head < tail)
    {
        for (uint32_t i = head; i < tail; i++)
        {
            if (NAME == "L1D_WQ")
            {
                if (entry[i].full_addr == packet->full_addr)
                {
                    DP(if (warmup_complete[packet->cpu])
                       {
                           cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                           cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                           cout << " cycle " << packet->event_cycle << endl;
                       });
                    return i;
                }
            }
            else
            {
                if (entry[i].address == packet->address)
                {
                    DP(if (warmup_complete[packet->cpu])
                       {
                           cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                           cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                           cout << " cycle " << packet->event_cycle << endl;
                       });
                    return i;
                }
            }
        }
    }
    else
    {
        for (uint32_t i = head; i < SIZE; i++)
        {
            if (NAME == "L1D_WQ")
            {
                if (entry[i].full_addr == packet->full_addr)
                {
                    DP(if (warmup_complete[packet->cpu])
                       {
                           cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                           cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                           cout << " cycle " << packet->event_cycle << endl;
                       });
                    return i;
                }
            }
            else
            {
                if (entry[i].address == packet->address)
                {
                    DP(if (warmup_complete[packet->cpu])
                       {
                           cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                           cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                           cout << " cycle " << packet->event_cycle << endl;
                       });
                    return i;
                }
            }
        }
        for (uint32_t i = 0; i < tail; i++)
        {
            if (NAME == "L1D_WQ")
            {
                if (entry[i].full_addr == packet->full_addr)
                {
                    DP(if (warmup_complete[packet->cpu])
                       {
                           cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                           cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                           cout << " cycle " << packet->event_cycle << endl;
                       });
                    return i;
                }
            }
            else
            {
                if (entry[i].address == packet->address)
                {
                    DP(if (warmup_complete[packet->cpu])
                       {
                           cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                           cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                           cout << " cycle " << packet->event_cycle << endl;
                       });
                    return i;
                }
            }
        }
    }

    return -1;
}

/*
int PACKET_QUEUE::check_queue(PACKET *packet)
{
    if ((head == tail) && occupancy == 0)
        return -1;

    if (head < tail) {
        for (uint32_t i=head; i<tail; i++) {
            if (NAME == "L1D_WQ") {
                if (entry[i].full_addr == packet->full_addr) {
                    DP (if (warmup_complete[packet->cpu]) {
                    cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                    cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                    cout << " cycle " << packet->event_cycle << endl; });
                    return i;
                }
            }
            else {
                if (entry[i].address == packet->address) {
                    DP (if (warmup_complete[packet->cpu]) {
                    cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                    cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                    cout << " cycle " << packet->event_cycle << endl; });
                    return i;
                }
            }
        }
    }
    else {
        for (uint32_t i=head; i<SIZE; i++) {
            if (NAME == "L1D_WQ") {
                if (entry[i].full_addr == packet->full_addr) {
                    DP (if (warmup_complete[packet->cpu]) {
                    cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                    cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                    cout << " cycle " << packet->event_cycle << endl; });
                    return i;
                }
            }
            else {
                if (entry[i].address == packet->address) {
                    DP (if (warmup_complete[packet->cpu]) {
                    cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                    cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                    cout << " cycle " << packet->event_cycle << endl; });
                    return i;
                }
            }
        }
        for (uint32_t i=0; i<tail; i++) {
            if (NAME == "L1D_WQ") {
                if (entry[i].full_addr == packet->full_addr) {
                    DP (if (warmup_complete[packet->cpu]) {
                    cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                    cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                    cout << " cycle " << packet->event_cycle << endl; });
                    return i;
                }
            }
            else {
                if (entry[i].address == packet->address) {
                    DP (if (warmup_complete[packet->cpu]) {
                    cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id << " same address: " << hex << packet->address;
                    cout << " full_addr: " << packet->full_addr << dec << " by instr_id: " << entry[i].instr_id << " index: " << i;
                    cout << " cycle " << packet->event_cycle << endl; });
                    return i;
                }
            }
        }
    }

    return -1;
}
*/

void PACKET_QUEUE::add_queue(PACKET *packet)
{

#ifdef DEBUG_ARTHUR
    if (NAME == "NTB_WQ")
    {
        for (uint32_t j = 0; j < SIZE; j++)
        {
            cout << "NTB WQ recebeu add " << j << " instr_id: " << packet->instr_id << " address: " << packet->address << " full_address: " << packet->full_addr << endl;
        }
    }
#endif

#ifdef SANITY_CHECK
    if (occupancy && (head == tail))
        assert(0);
#endif

    // add entry
    entry[tail] = *packet;

    DP(if (warmup_complete[packet->cpu])
       {
           cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id;
           cout << " address: " << hex << entry[tail].address << " full_addr: " << entry[tail].full_addr << dec;
           cout << " head: " << head << " tail: " << tail << " occupancy: " << occupancy << " event_cycle: " << entry[tail].event_cycle << endl;
       });

    occupancy++;
    tail++;
    if (tail >= SIZE)
        tail = 0;
}

void PACKET_QUEUE::remove_queue(PACKET *packet)
{
#ifdef DEBUG_ARTHUR
    if (NAME == "NTB_WQ")
        cout << "removed do WQ do NTB, address: " << packet->address << "  instr_id: " << packet->instr_id << " cycle: " << current_core_cycle[packet->cpu] << endl;

#endif
#ifdef SANITY_CHECK
    if ((occupancy == 0) && (head == tail))
        assert(0);
#endif

    DP(if (warmup_complete[packet->cpu])
       {
           cout << "[" << NAME << "] " << __func__ << " cpu: " << packet->cpu << " instr_id: " << packet->instr_id;
           cout << " address: " << hex << packet->address << " full_addr: " << packet->full_addr << dec << " fill_level: " << packet->fill_level;
           cout << " head: " << head << " tail: " << tail << " occupancy: " << occupancy << " event_cycle: " << packet->event_cycle << endl;
       });

    // reset entry
    PACKET empty_packet;
    *packet = empty_packet;

    occupancy--;
    head++;
    if (head >= SIZE)
        head = 0;
}
