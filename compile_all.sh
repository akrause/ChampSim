#!/bin/bash
outsidewsl=0
incfile=inc/cache.h
if grep -qi Microsoft /proc/version; then
	echo "Ubuntu on Windows"
	echo `pwd -P` | grep mnt > /dev/null
	if [ $? -eq 0 ]; then
		echo "Directory outside wsl"
		outsidewsl=1
		dir=`pwd`
		rm -rf /tmp/ChampSim/
		cp -r ../ChampSim/ /tmp/
		cd /tmp/ChampSim/
	else
		echo "Directory inside wsl"
fi
else
  echo "Native Linux"
fi
declare -a caches=("1024" "2048" "8196" "832"  "1744" "7168")
declare -a sets=("1024" "2048" "8196" "1024" "2048" "8196")
declare -a ways=("16"   "16"   "16"   "13"   "13"   "14")
cachenum=${#caches[@]}

for index in `seq 0 $(($cachenum - 1))`; do
for replacement in lru srrip drrip ship; do
#for replacement in lru; do
	for storent in 1 0; do
	#for storent in 1 ; do
		for loadnt in 1 0; do
		#for loadnt in 1 ; do
			for bypassreq in 1 0; do
			#for bypassreq in 1 ; do
				for bypassfill in 1 0; do
				#for bypassfill in 1; do
					if  ([ "$storent" == "0" ] && [ "$loadnt" == "0" ]) && ([ "$bypassfill" == "1" ] || [ "$bypassreq" == "1" ]); then
						:
					elif ([ "$storent" == "1" ] ||  [ "$loadnt" == "1" ]) &&  [ "$bypassreq" == "0" ] &&  [ "$bypassfill" == "0" ]; then
						:
					elif ([ "$storent" == "0" ] || [ "$loadnt" == 0 ]) && ([ "$bypassreq" == "1" ] || [ "$bypassfill" == "1" ]) ; then
						:
					elif ([ "$storent" == "0" ] && [ "$index" -ge "3" ]); then
						:
					else
						filename=""
						sed -i "s/#define STORE_NT.*/#define STORE_NT $storent/g" $incfile
						sed -i "s/#define LOAD_NT.*/#define LOAD_NT $loadnt/g" $incfile
						sed -i "s/#define BYPASS_REQ.*/#define BYPASS_REQ $bypassreq/g" $incfile
						sed -i "s/#define BYPASS_FILL.*/#define BYPASS_FILL $bypassfill/g" $incfile
						sed -i "s/#define LLC_SET NUM_CPUS.*/#define LLC_SET NUM_CPUS*${sets[$index]}/g" $incfile
						sed -i "s/#define LLC_WAY.*/#define LLC_WAY ${ways[$index]}/g" $incfile
						echo "Building STORE_NT: $storent, LOAD_NT: $loadnt, BYPASS_REQ: $bypassreq, BYPASS_FILL: $bypassfill, SETS: ${sets[$index]}, WAYS: ${ways[$index]}"
						
						error=1
						while [ $error -eq 1 ]; do
							./build_champsim.sh bimodal no no no no no $replacement 1
							error=$?
						done
						error=1
						filename=nn.$storent$loadnt$bypassreq$bypassfill.$replacement.${caches[$index]}
						mv bin/bimodal-no-no-no-no-no-$replacement-1core bin/$filename

						while [ $error -eq 1 ]; do
							./build_champsim.sh bimodal next_line next_line ip_stride next_line no $replacement 1
							error=$?
						done
						error=1


						filename=pn.$storent$loadnt$bypassreq$bypassfill.$replacement.${caches[$index]}
						mv bin/bimodal-next_line-next_line-ip_stride-next_line-no-$replacement-1core bin/$filename
						
						if [ "$storent" == 1 ]; then 
							while [ $error -eq 1 ]; do
								./build_champsim.sh bimodal no no no no ip_stride  $replacement 1
								error=$?
							done
							error=1



							filename=np.$storent$loadnt$bypassreq$bypassfill.$replacement.${caches[$index]}
							mv bin/bimodal-no-no-no-no-ip_stride-$replacement-1core bin/$filename

							while [ $error -eq 1 ]; do
								./build_champsim.sh bimodal next_line next_line ip_stride next_line ip_stride $replacement 1
								error=$?
							done
							error=1

							filename=pp.$storent$loadnt$bypassreq$bypassfill.$replacement.${caches[$index]}
							mv bin/bimodal-next_line-next_line-ip_stride-next_line-ip_stride-$replacement-1core bin/$filename
						fi
					fi
				done
			done
		done
	done
done
done
if [ $outsidewsl -eq 1 ]; then
	cp -r bin $dir
fi
