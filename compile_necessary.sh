#!/bin/bash
outsidewsl=0
incfile=inc/cache.h
if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters"
    exit 1
fi
if grep -qi Microsoft /proc/version; then
	echo "Ubuntu on Windows"
	echo `pwd -P` | grep mnt > /dev/null
	if [ $? -eq 0 ]; then
		echo "Directory outside wsl"
		outsidewsl=1
		dir=`pwd`
		rm -rf /tmp/ChampSim/
		cp -r ../ChampSim/ /tmp/
		cd /tmp/ChampSim/
	else
		echo "Directory inside wsl"
fi
else
  echo "Native Linux"
fi
sets=${1}
ways=${2}
name=${3}
#for cachesize in 1024; do
#for cachesize in 128 512 1024 1280 8192 131072; do
#for replacement in lru srrip drrip ship; do
for replacement in lru; do
	#for storent in 1 0; do
	for storent in 1 ; do
		#for loadnt in 1 0; do
		for loadnt in 1 ; do
			#for bypassreq in 1 0; do
			for bypassreq in 1 ; do
				#for bypassfill in 1 0; do
				for bypassfill in 1; do
					if  ([ "$storent" == "0" ] && [ "$loadnt" == "0" ]) && ([ "$bypassfill" == "1" ] || [ "$bypassreq" == "1" ]); then
						:
					elif ([ "$storent" == "1" ] ||  [ "$loadnt" == "1" ]) &&  [ "$bypassreq" == "0" ] &&  [ "$bypassfill" == "0" ]; then
						:
					elif ([ "$storent" == "0" ] || [ "$loadnt" == 0 ]) && ([ "$bypassreq" == "1" ] || [ "$bypassfill" == "1" ]) ; then
						:
					else
						filename=""
						sed -i "s/#define STORE_NT.*/#define STORE_NT $storent/g" $incfile
						sed -i "s/#define LOAD_NT.*/#define LOAD_NT $loadnt/g" $incfile
						sed -i "s/#define BYPASS_REQ.*/#define BYPASS_REQ $bypassreq/g" $incfile
						sed -i "s/#define BYPASS_FILL.*/#define BYPASS_FILL $bypassfill/g" $incfile
						sed -i "s/#define LLC_SET NUM_CPUS.*/#define LLC_SET NUM_CPUS*$sets/g" $incfile
						sed -i "s/#define LLC_WAY.*/#define LLC_WAY $ways/g" $incfile
						echo "Building STORE_NT: $storent, LOAD_NT: $loadnt, BYPASS_REQ: $bypassreq, BYPASS_FILL: $bypassfill, SETS: $sets, WAYS: $ways"
						./build_champsim.sh bimodal no no no no no $replacement 1
						if [ $? -eq 1 ]; then
							echo "BUILD FAILED"
							exit 1
						fi
						filename=$storent$loadnt$bypassreq$bypassfill$replacement$name
						mv bin/bimodal-no-no-no-no-no-$replacement-1core bin/$filename
						./build_champsim.sh bimodal no no no no ip_stride $replacement 1
						if [ $? -eq 1 ]; then
							echo "BUILD FAILED"
							exit 1
						fi
						filename=$storent$loadnt$bypassreq$bypassfill$replacement$name\ntbpref
						mv bin/bimodal-no-no-no-no-ip_stride-$replacement-1core bin/$filename
					fi
				done
			done
		done
	done
echo "Building Prefetcher. SETS: $sets, WAYS: $ways"
./build_champsim.sh bimodal next_line next_line ip_stride next_line ip_stride $replacement 1
if [ $? -eq 1 ]; then
	echo "BUILD FAILED"
	exit 1
fi
mv bin/bimodal-next_line-next_line-ip_stride-next_line-ip_stride-$replacement-1core bin/p$replacement$name
done
if [ $outsidewsl -eq 1 ]; then
	cp -r bin $dir
fi
